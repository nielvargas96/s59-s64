// import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext';

import Header from './components/Header';
import Footer from './components/Footer';
import Products from './pages/Products';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
import AdminDashboard from './pages/AdminDashboard';
import ProductView from './components/ProductView';
import RetrieveAllUsers from './admin/RetrieveAllUsers';
import RetrieveAllOrders from './admin/RetrieveAllOrders';
import OrderedProduct from './components/OrderedProduct';
import Checkout from './components/Checkout';



function App() {
  const [user, setUser] = useState(
    {
      id: null,
      isAdmin: null,
      email: null
    }
  );

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/users/details`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email
        })

        // console.log(data)
        // console.log(`isAdmin: ${data.isAdmin}`)
      })
  }, []);


  return (
    <div className="App">
      <UserProvider value={{ user, setUser, unsetUser }}>
        <BrowserRouter>
          <Header />
          <main className='mt-[80px] min-h-[80vh]  bg-gray-100 dark:bg-gradient-to-b from-gray-900 to-gray-700' >
            <section className='max-w-7xl mx-auto pt-[4rem] pb-[10rem] px-3'>
              <Routes>
                {/* <Route path="/home" element={<Home />}> </Route> */}
                <Route path="/" element={<Products />}> </Route>
                <Route path="/login" element={<Login />}> </Route>
                <Route path="/register" element={<Register />}> </Route>
                <Route path="/logout" element={<Logout />}> </Route>
                <Route path="/admin" element={<AdminDashboard />}> </Route>
                <Route path="/product/:id" element={<ProductView />}> </Route>
                <Route path="/retrieveUsers" element={<RetrieveAllUsers />}> </Route>
                <Route path="/retrieveOrders" element={<RetrieveAllOrders />}> </Route>
                <Route path="/myorder" element={<OrderedProduct />}> </Route>
                <Route path="/checkout/:id" element={<Checkout />}> </Route>

                <Route path='*' element={<PageNotFound />} />
              </Routes>
            </section>
          </main>
          <Footer />
        </BrowserRouter>
      </UserProvider>
    </div>
  );
}

export default App;
