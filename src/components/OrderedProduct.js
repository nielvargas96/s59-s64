import React, { useEffect, useContext, useState } from 'react'
import UserContext from '../UserContext';
import Swal2 from 'sweetalert2'
import PageNotFound from '../pages/PageNotFound';

const OrderedProduct = () => {
  const { user } = useContext(UserContext);

  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/users/details`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data)

        const userOrders = data.orderProducts || [];
        setOrders(userOrders);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }, []);

  return (
    user.id === null || user.id === undefined || user.isAdmin ? (
      <PageNotFound />
    ) : (
      <>
        <div className="relative overflow-x-auto px-6 ">
          <h1 className="text-[2.5rem] mb-[1.5rem] text-left px-[2rem] font-semibold dark:text-gray-200">My Orders</h1>
          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead className="text-xs  uppercase bg-gray-500 text-gray-200 dark:bg-gray-700 dark:text-gray-400">
              <tr>
                <th className="px-6 py-3">
                  No.
                </th>
                <th className="px-6 py-3">
                  Product ID
                </th>
                <th className="px-6 py-3">
                  Product Name
                </th>
                <th className="px-6 py-3">
                  Quantity
                </th>
                <th className="px-6 py-3">
                  Price
                </th>
                <th className="px-6 py-3">
                  Total
                </th>
                <th className="px-6 py-3">
                  Date
                </th>
                <th className="px-6 py-3">
                  Status
                </th>
              </tr>
            </thead>
            <tbody>
              {orders.length > 0 ? (
                orders.map((order, index) => (
                  <tr key={index} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <td className="px-6 py-4  text-gray-600 bg-gray-300 dark:bg-gray-800 whitespace-nowrap  dark:text-white">
                      {index}
                    </td>
                    <td className="px-6 py-4  text-gray-600 bg-gray-300 dark:bg-gray-800 whitespace-nowrap dark:text-white">
                      {order._id}
                    </td>
                    <td className="px-6 py-4  text-gray-600 bg-gray-300 dark:bg-gray-800 whitespace-nowrap dark:text-white">
                      {order.name}
                    </td>
                    <td className="px-6 py-4  text-gray-600 bg-gray-300 dark:bg-gray-800 whitespace-nowrap dark:text-white">
                      {order.quantity}
                    </td>
                    <td className="px-6 py-4  text-gray-600 bg-gray-300 dark:bg-gray-800 whitespace-nowrap dark:text-white">
                      &#8369;{(order.price).toLocaleString()}
                    </td>
                    <td className="px-6 py-4  text-gray-600 bg-gray-300 dark:bg-gray-800 whitespace-nowrap dark:text-white">
                      &#8369;{(order.quantity * order.price).toLocaleString()}
                    </td>
                    <td className="px-6 py-4  text-gray-600 bg-gray-300 dark:bg-gray-800 whitespace-nowrap dark:text-white">
                      {new Date(order.orderedDate).toLocaleString()}
                    </td>
                    <td className="px-6 py-4  text-gray-600 bg-gray-300 dark:bg-gray-800 whitespace-nowrap dark:text-white">
                      {order.status}
                    </td>
                  </tr>
                )
                )
              ) : (
                <tr>
                  <td><p>No ordered products found.</p></td>
                </tr>
              )}

            </tbody>
          </table>
        </div>
      </>
    )
  );
}

export default OrderedProduct;