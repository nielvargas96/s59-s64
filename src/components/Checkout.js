import React, { useState, useEffect, useContext } from 'react'
import { useParams, useNavigate, Link } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal2 from 'sweetalert2'


const Checkout = () => {

  const navigate = useNavigate();
  const [name, setProductName] = useState('');
  const [price, setPrice] = useState('');
  const [description, setDescription] = useState('');
  const [image, setImage] = useState('');

  const { user } = useContext(UserContext);

  const firstName = user.firstName;
  const lastName = user.lastName;
  const email = user.email;

  // This is from our URL in the App.JS
  const { id } = useParams();
  // console.log(id)

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/products/${id}`)
      .then(response => response.json())
      .then(data => {
        setProductName(data.name)
        setPrice(data.price)
        setDescription(data.description)
        setImage(data.image)
      })
  }, [])


  // Increment decrement qty
  const [countQty, setCountQty] = useState(parseInt(localStorage.getItem('addcart_qty')) || 1);

  const handleIncrement = () => {
    if (countQty < 50) {
      setCountQty(prevCount => prevCount + 1);
    }
  };

  const handleDecrement = () => {
    if (countQty > 1) {
      setCountQty(prevCount => prevCount - 1);
    }
  };

  const handleQtyChange = (e) => {
    const value = Number((e).target.value);
    if (!isNaN(value) && value >= 1 && value <= 50) {
      setCountQty(value);
    }
  };

  const checkout = (productId) => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/users/addCart`, {
      method: "POST",
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        productId: `${productId}`,
        userId: `${user.id}`,
        name: `${name}`,
        price: `${price}`,
        quantity: `${countQty}`,
        firstName: `${firstName}`,
        lastName: `${lastName}`,
        email: `${email}`
      })
    })
      .then(response => response.json())
      .then(data => {
        console.log(data)
        if (data === true) {
          Swal2.fire({
            icon: 'success',
            title: 'Ordered successful',
            // text: `${data}`
          }).then(() => {
            localStorage.removeItem('addcart_qty');
            navigate('/myorder');
          });
        } else {
          Swal2.fire({
            icon: 'error',
            title: 'You need to login first to add this cart.',
          }).then(() => {
            navigate('/login');
          });
        }
      });
  };

  //   productId: id,
  //   quantity: countQty // Include the quantity value here
  // };
  // const checkout = (productId) => {
  //   // const payload = {
  //   //   productId: id,
  //   //   quantity: countQty // Include the quantity value here
  //   // };

  //   fetch(`${process.env.REACT_APP_BACKEND_API}/users/addCart`, {
  //     method: "POST",
  //     headers: {
  //       Authorization: `Bearer ${localStorage.getItem('token')}`,
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify({
  //       productId: `${productId}`
  //     })
  //   })
  //     .then(response => response.json())
  //     .then(data => {
  //       Swal2.fire({
  //         icon: 'success',
  //         title: 'Enrollment successful',
  //         text: `${data}`
  //       });
  //       // if (data === true) {
  //       //   Swal2.fire({
  //       //     icon: 'success',
  //       //     title: 'Enrollment successful',
  //       //     text: `${data}`
  //       //   });
  //       // } else {
  //       //   Swal2.fire({
  //       //     icon: 'error',
  //       //     title: 'You need to login first to add this cart.',
  //       //   }).then(() => {
  //       //     navigate('/login');
  //       //   });
  //       // }
  //     });
  // };

  return (
    <div className="md:flex items-start justify-center py-12 2xl:px-6 md:px-6 px-4 ">
      <div className=" md:w-1/2 p-[2rem] bg-gray-200 rounded-xl dark:bg-gray-800 shadow-xl" >
        <h2 className="text-[2.75rem] text-slate-800 dark:text-white/80 font-extrabold mb-[2rem]">
          CHECKOUT ORDER:
        </h2>

        <div className="border-b border-gray-500">

          <h2 className="text-[1.75rem] font-semibold  text-slate-700 dark:text-white/80 py-4 uppercase ">
            {name}
          </h2>
        </div>
        <div className="py-4 border-b border-gray-500 flex items-center ">
          <p className="text-[1.5rem]   text-gray-700 dark:text-white/80 flex justify-between w-full">PRICE: <span className='ml-auto font-semibold'>&#8369;{price.toLocaleString()}</span></p>
        </div>

        <div className="py-4 border-b border-gray-500 flex items-center  justify-between">
          <p className="text-[1.5rem]   text-gray-700 dark:text-white/80  ">QTY:</p>
          <div className="flex flex-row h-10 w-auto rounded-lg relative bg-transparent mt-1 ">
            <button onClick={() => handleDecrement()} className=" bg-gray-200 text-gray-700 hover:text-gray-700 hover:bg-gray-400 h-full w-10 rounded-l cursor-pointer outline-none">
              <span className="m-auto text-2xl font-thin">−</span>
            </button>
            <input value={countQty} onChange={(e) => handleQtyChange(e)} min={1} type="number" className="  focus:outline-none text-center w-20 bg-gray-200 font-semibold text-md hover:text-black focus:text-black  md:text-basecursor-default flex items-center text-gray-700  outline-0" name="custom-input-number" />
            <button onClick={() => handleIncrement()} className="bg-gray-200 text-gray-700 hover:text-gray-700 hover:bg-gray-400 h-full w-10 rounded-r cursor-pointer">
              <span className="m-auto text-2xl font-thin">+</span>
            </button>
          </div>

          <span className='text-[1.5rem]   text-gray-700 dark:text-white/80 font-semibold'><small>x</small>{countQty}</span>
        </div>
        <div>
          <p className=" text-gray-600 dark:text-white/80 mt-3 mb-8 text-[1.5rem] flex justify-between">
            TOTAL AMOUNT: <span className='ml-auto font-semibold'>&#8369;{(countQty * price).toLocaleString()}</span>
          </p>
        </div>

        {/* <button onClick={() => checkout(`${id}`)} className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 text-base flex items-center justify-center leading-none text-white bg-gray-800 w-full py-4 hover:bg-gray-700">
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-cart-plus mr-2" viewBox="0 0 16 16">
            <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z" />
            <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
          </svg>
          Add to cart User
        </button> */}


        {
          (user.id === null || user.id === undefined) ? (
            <Link to="/login" className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 text-base flex items-center justify-center leading-none text-white bg-gray-800 w-full py-4 hover:bg-gray-700">
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-cart-plus mr-2" viewBox="0 0 16 16">
                <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z" />
                <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
              </svg>
              Add to cart
            </Link>
          ) : (
            (user.isAdmin == true) ? (
              // Nested ternary operator condition
              <button className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 text-base flex items-center justify-center leading-none text-white bg-gray-500 w-full py-4 hover:bg-gray-400 cursor-not-allowed">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-cart-plus mr-2" viewBox="0 0 16 16">
                  <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z" />
                  <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                </svg>
                Disabled for Admin
              </button>
            ) : (
              <button onClick={() => checkout(`${id}`)} className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 text-base flex items-center justify-center leading-none text-white bg-gray-900 w-full py-4 hover:bg-gray-700">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-cart-plus mr-2" viewBox="0 0 16 16">
                  <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z" />
                  <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                </svg>
                PLACE YOUR ORDER
              </button>
            )
          )
        }


      </div>
    </div >
  )
}

export default Checkout