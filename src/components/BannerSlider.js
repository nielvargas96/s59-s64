import React, { useEffect } from 'react';
// import Swiper JS
import Swiper from 'swiper';
// import Swiper styles
import 'swiper/css';
import banner1 from '../assets/img/banner_1.png'
import banner2 from '../assets/img/banner_2.png'
import banner3 from '../assets/img/banner_3.png'




const BannerSlider = () => {
  useEffect(() => {
    const swiper = new Swiper('.swiper-container', {
      slidesPerView: 1,
      loop: true,
      autoplay: {
        delay: 1000,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      }, pagination: {
        el: ".swiper-pagination",
      }
    });

  }, []);

  return (
    <div className='w-100  mb-[4rem] overflow-hidden max-h-[360px]'>
      <div className="swiper-container ">
        <div className="swiper-wrapper">
          <div className="swiper-slide">
            <img src={banner1} />
          </div>
          <div className="swiper-slide">
            <img src={banner2} />
          </div>
          <div className="swiper-slide">
            <img src={banner3} />
          </div>
        </div>
        <div className="swiper-button-next"></div>
        <div className="swiper-button-prev"></div>
        <div className="swiper-pagination"></div>
      </div>
    </div>
  )
}

export default BannerSlider