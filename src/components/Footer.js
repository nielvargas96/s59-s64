import React, { useState, useEffect } from 'react'
import logoFooter from '../assets/img/logo_gray.png'

const Footer = () => {


  return (
    <footer className="bg-gray-100  dark:bg-gray-900" >
      <hr className="border-gray-200 sm:mx-auto dark:border-gray-700 " />
      <div className="w-full max-w-screen-xl mx-auto px-4 pt-[4.5rem] pb-[2.5rem]">
        <div className="flex justify-center items-center flex-col sm:flex-row sm:items-center sm:justify-between">
          <p className="flex items-center mb-6 sm:mb-0">
            {/* {isDarkMode ? */}
            <img src={logoFooter} alt='logo' className="max-w-[50] sm:max-w-full brightness-100 dark:brightness-[100]" width={200} />
            {/* : */}
            {/* <img src={logo} alt='logo' className="max-w-[50] sm:max-w-full brightness-100 dark:brightness-[100]" width={250} /> */}
            {/* } */}
          </p>
          {/* <ul className="flex flex-wrap items-center  text-sm font-medium text-gray-500 sm:mb-0 dark:text-gray-400">
            <li>
              <a className="mr-4 hover:underline md:mr-6 ">Products</a>
            </li>
            <li>
              <a className="mr-4 hover:underline md:mr-6">Contact Us</a>
            </li>
            <li>
              <a className="mr-4 hover:underline md:mr-6 ">Login</a>
            </li>
            <li>
              <a className="hover:underline">Register</a>
            </li>
          </ul> */}
        </div>
        <hr className="my-6 border-gray-200 sm:mx-auto dark:border-gray-700 lg:my-8" />
        <span className="block text-center text-sm text-gray-500 sm:text-center dark:text-gray-400">© 2023 <a className="hover:underline">SHOPN'KICKS</a>. All Rights Reserved.</span>
      </div>
    </footer >
  )
}

export default Footer