import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
// import UserContext from '../UserContext';

const ProductCard = ({ productProp }) => {

  const { _id, name, description, price, image } = productProp;

  // const { user } = useContext(UserContext);
  const scrollToTop = () => {
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
  };

  return (
    <>
      <div className="w-full rounded-md overflow-hidden drop-shadow-[0_2px_8px_rgba(0,0,0,0.25)] m-0 sm-m-4 lg:max-w-sm bg-gray-100  dark:bg-gray-800 " key="">
        <div className=' rounded-md bg-gray-200  hover:opacity-75'>
          <Link to={`/product/${_id}`} onClick={() => { scrollToTop() }} className='cursor-pointer'>
            <img
              className="object-cover w-full h-100"
              src={image}
              alt={name}
            />
          </Link>
        </div>
        <div className="pt-[1rem] pb-[2rem] px-6">
          <h4 className="text-[1.25rem] font-semibold text-slate-900 dark:text-slate-100 mb-2">
            {name}
          </h4>
          <p className="mb-1 text-[.875rem] dark:text-gray-100	">
            {description}
          </p>
          <p className="mb-6  text-[.875rem] overflow-hidden text-ellipsis	dark:text-gray-100">
            &#8369;{(price).toLocaleString()}
          </p>
          <Link to={`/product/${_id}`} onClick={() => { scrollToTop() }} className=" text-base flex items-center justify-center leading-none text-white bg-gray-800 w-full py-4  hover:bg-gray-700 dark:text-white dark:bg-gray-900 dark:hover:bg-gray-900/50">
            View Details
          </Link>
          {/* {
            (user.id == null || user.id == "undefined")
              ?
            
              :
              <Link to={`/login`} className="px-6 py-1.5 text-sm text-white  rounded shadow bg-blue-500 hover:bg-blue-700/95">
                View Details
              </Link>
          } */}

        </div>
      </div>


    </>
  )
}

export default ProductCard