import React, { useContext, useEffect, useState } from 'react'
import logo from '../assets/img/logo_gray.png'
import { Link, useNavigate } from 'react-router-dom';
import Swal2 from 'sweetalert2'
import UserContext from '../UserContext';
import PageNotFound from './PageNotFound';

const Login = () => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const navigate = useNavigate();

  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    console.log(user)
  }, [])

  function SignIn(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_BACKEND_API}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(response => response.json())
      .then(data => {
        if (!data) {
          Swal2.fire({
            icon: 'error',
            title: `Invalid login`
          })
        } else {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access)
          // Swal2.fire({
          //   icon: 'success',
          //   title: 'Login Sucess',
          //   text: 'Welcome!'
          // })
        }
      })
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/users/details`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then(data => {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        if (data.isAdmin) {
          Swal2.fire({
            icon: 'success',
            title: 'Login Success',
            text: 'Welcome Admin!'
          });
          navigate('/admin');
        } else {
          Swal2.fire({
            icon: 'success',
            title: 'Login Success',
            text: `Welcome User!`
          });
          navigate('/');
        }
      })
  }

  return (
    user.id === null || user.id === undefined
      ?
      <section >
        <div className="flex flex-col items-center justify-top px-6 py-8 mx-auto  lg:py-0">
          <p className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white">
            <img width={240} src={logo} alt="logo" className='brightness-100 dark:brightness-[100]' />
          </p>

          <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700 drop-shadow-[0_2px_8px_rgba(0,0,0,0.25)]">
            <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
              <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                Login your account
              </h1>

              <form onSubmit={(e) => SignIn(e)} className="space-y-4 md:space-y-6" >
                <div>
                  <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your email</label>
                  <input value={email} onChange={e => setEmail(e.target.value)} type="email" name="email" id="email" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="example@mail.com" required />
                </div>

                <div>
                  <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
                  <input value={password} onChange={e => setPassword(e.target.value)} type="password" name="password" id="password" placeholder="••••••••" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required />
                </div>

                <button type="submit" className="w-full text-white bg-blue-600 hover:bg-blue-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">Sign in</button>

                <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                  Don’t have an account yet? <Link className="font-medium text-blue-600 hover:underline dark:text-primary-500" to="/register">Sign up</Link>
                </p>
              </form>
            </div>
          </div>
        </div>
      </section>
      :
      <PageNotFound />
  )
}

export default Login