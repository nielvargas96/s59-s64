import React, { useState, useContext, useEffect } from 'react'
import UserContext from '../UserContext';
import PageNotFound from './PageNotFound';
import ProductItem from '../admin/ProductItem'
import Swal2 from 'sweetalert2'
import { Link } from 'react-router-dom'

const AdminDashboard = () => {
  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([])
  const [productAdded, setProductAdded] = useState(false);

  // Create new product FETCH
  function createProduct(productData) {
    fetch(`${process.env.REACT_APP_BACKEND_API}/products/addNewProduct`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(productData)
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        // Handle success response
        console.log('Product added successfully:', data);
        setProductAdded(prevState => !prevState); // Toggle the productAdded state to trigger the useEffect
      })
      .catch(error => {
        // Handle error response
        console.error('Error adding product:', error);
      });
  }

  // Create new product
  function addNewProduct(e) {
    e.preventDefault();
    Swal2.fire({
      title: 'Add New Product',
      html: `
    <input type="text" id="productName" style="width: 80%;" class="swal2-input text-[1rem]" placeholder="Product Name" required>
    <input type="text" id="description" style="width: 80%;" class="swal2-input text-[1rem]" placeholder="Description" required>
    <input type="number" id="price" style="width: 80%;" class="swal2-input text-[1rem]" placeholder="Price" required>
    <input type="text" id="image" style="width: 80%;" class="swal2-input text-[1rem]" placeholder="Image URL" required>
  `,
      showCancelButton: true,
      confirmButtonText: 'Add',
      preConfirm: () => {
        const productName = document.getElementById('productName').value;
        const description = document.getElementById('description').value;
        const price = document.getElementById('price').value;
        const image = document.getElementById('image').value;

        // Check if all fields are filled
        if (productName && description && price && image) {
          createProduct({
            name: productName,
            description: description,
            price: price,
            image: image
          });
        } else {
          Swal2.showValidationMessage('Please fill in all fields');
        }
      }
    });
  }


  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/products/all`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        // Filter out inactive products
        // const filteredProducts = data.filter(product => product.isActive); 
        // setProducts(filteredProducts.map(product => (
        //   <ProductItem key={product._id} productProp={product} />
        // )));
        setProducts(data.map(product => (
          <ProductItem key={product._id} productProp={product} />
        )));
      })
      .catch(error => console.error(error));
  }, [productAdded, products]); // Trigger useEffect when productAdded or products change


  // View all products
  // retrieveAllUsers

  return (
    user.isAdmin === true
      ?
      <div className='mt-4'>
        <div className='flex justify-between items-center'>
          <h1 className="text-[2.5rem]  text-left px-[2rem] font-semibold dark:text-gray-200">Dashboard Admin</h1>
          <div className='ml-auto flex px-2'>
            <button onClick={(e) => addNewProduct(e)} className="  bg-emerald-700 hover:bg-emerald-600 text-white font-semibold hover:text-white py-1 px-4 mr-2 border border-emerald-500  rounded">
              <span className='text-[1rem] font-bold'>+</span> Add Product
            </button>

            <Link to='/retrieveOrders' className="ml-auto flex items-center bg-emerald-700 hover:bg-emerald-600 text-white font-semibold hover:text-white py-1 px-4 mr-2 border border-emerald-500  rounded">
              <svg className="w-4 h-4 text-white dark:text-white mr-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 19 20">
                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 15a2 2 0 1 0 0 4 2 2 0 0 0 0-4Zm0 0h8m-8 0-1-4m9 4a2 2 0 1 0 0 4 2 2 0 0 0 0-4Zm1-4H5m0 0L3 4m0 0h5.501M3 4l-.792-3H1m11 3h6m-3 3V1" />
              </svg> Orders
            </Link>

            <Link to='/retrieveUsers' className="ml-auto flex items-center bg-emerald-700 hover:bg-emerald-600 text-white font-semibold hover:text-white py-1 px-4 mr-2 border border-emerald-500  rounded">
              <svg className="w-4 h-4 mr-2 text-white dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 19"> <path d="M14.5 0A3.987 3.987 0 0 0 11 2.1a4.977 4.977 0 0 1 3.9 5.858A3.989 3.989 0 0 0 14.5 0ZM9 13h2a4 4 0 0 1 4 4v2H5v-2a4 4 0 0 1 4-4Z" /> <path d="M5 19h10v-2a4 4 0 0 0-4-4H9a4 4 0 0 0-4 4v2ZM5 7a5.008 5.008 0 0 1 4-4.9 3.988 3.988 0 1 0-3.9 5.859A4.974 4.974 0 0 1 5 7Zm5 3a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm5-1h-.424a5.016 5.016 0 0 1-1.942 2.232A6.007 6.007 0 0 1 17 17h2a1 1 0 0 0 1-1v-2a5.006 5.006 0 0 0-5-5ZM5.424 9H5a5.006 5.006 0 0 0-5 5v2a1 1 0 0 0 1 1h2a6.007 6.007 0 0 1 4.366-5.768A5.016 5.016 0 0 1 5.424 9Z" /> </svg> Users
            </Link>

          </div>
        </div>
        <div className="mt-4 -mb-3">
          <div className="not-prose relative bg-slate-50 rounded-xl overflow-hidden dark:bg-slate-800/25">
            <div className="relative rounded-xl overflow-auto">
              <div className="shadow-sm overflow-hidden mt-4">
                <table className="border-collapse table-fixed w-full text-sm">
                  <thead>
                    <tr>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Product Name</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Description</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={100}>Price</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={300}>Image</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={100}>Status</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Date Created</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={200}>Action</th>
                    </tr>
                  </thead>
                  <tbody className="bg-white dark:bg-slate-800">
                    {products}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      :
      <PageNotFound />
  )
}

export default AdminDashboard