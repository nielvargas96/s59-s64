import React from 'react'
import { Link } from 'react-router-dom'

const PageNotFound = () => {
  return (
    <>
      <h1 className='text-4xl text-center font-bold text-blue-950 dark:text-white mb-6 z'>Page not found!</h1>
      <p className='text-center text-black dark:text-white'>Click button below to go back to homepage. <br /><br />

        <Link to="/" className="px-6 py-3 text-sm text-white  rounded shadow bg-blue-500 hover:bg-blue-700/95">Back to homepage</Link></p>
    </>
  )
}

export default PageNotFound