import React, { useState, useContext, useEffect } from 'react'
import UserContext from '../UserContext';
import PageNotFound from './PageNotFound';
import ProductItem from '../admin/ProductItem'
import Swal2 from 'sweetalert2'

const AdminDashboard = () => {
  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([])
  const [productAdded, setProductAdded] = useState(false);

  // Display all product
  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/products/all`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        setProducts(data.map(product => {
          return (
            <ProductItem key={product._id} productProp={product} />
          )
        }))
      })
      .catch(error => console.error(error));
  }, []);

  // Create new product
  function addNewProduct(e) {
    e.preventDefault();
    Swal2.fire({
      title: 'Add New Product',
      html: `
        <input type="text" id="productName" class="swal2-input" placeholder="Product Name">
        <input type="text"  id="description" class="swal2-input" placeholder="Description">
        <input type="number" id="price" class="swal2-input" placeholder="Price">
        <input type="text"  id="image" class="swal2-input" placeholder="Image URL">
      `,
      showCancelButton: true,
      confirmButtonText: 'Add',
      preConfirm: () => {
        const productName = document.getElementById('productName').value;
        const description = document.getElementById('description').value;
        const price = document.getElementById('price').value;
        const image = document.getElementById('image').value;

        // Make the API call to add the new product using the fetched values
        fetch(`${process.env.REACT_APP_BACKEND_API}/products/addNewProduct`, {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            name: productName,
            description: description,
            price: price,
            image: image
          })
        })
          .then(response => {
            if (!response.ok) {
              throw new Error('Network response was not ok');
            }
            return response.json();
          })
          .then(data => {
            // Handle success response
            console.log('Product added successfully:', data);
            setProductAdded(prevState => !prevState); // Toggle the productAdded state
          })
          .catch(error => {
            // Handle error response
            console.error('Error adding product:', error);
          });
      }
    });

  }


  return (
    user.isAdmin === true
      ?
      <div>
        <h1 className="text-[2.5rem] mb-[1.5rem] text-left  font-semibold dark:text-gray-200">Dashboard Admin</h1>
        <div className='flex'>
          <button onClick={(e) => addNewProduct(e)} className="ml-auto  bg-emerald-700 hover:bg-emerald-600 text-white font-semibold hover:text-white py-1 px-4 mr-2 border border-emerald-500  rounded">
            <span className='text-[1.25rem] font-bold'>+</span> Add Product
          </button>
        </div>
        <div className="mt-4 -mb-3"><div className="not-prose relative bg-slate-50 rounded-xl overflow-hidden dark:bg-slate-800/25">
          <div className="relative rounded-xl overflow-auto">
            <div className="shadow-sm overflow-hidden my-8">
              <table className="border-collapse table-fixed w-full text-sm">
                <thead>
                  <tr>
                    <th className="border-b-2 dark:border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Product Name</th>
                    <th className="border-b-2 dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Description</th>
                    <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={100}>Price</th>
                    <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={300}>Image</th>
                    <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={100}>Status</th>
                    <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Date Created</th>
                    <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={200}>Action</th>
                  </tr>
                </thead>
                <tbody className="bg-white dark:bg-slate-800">
                  {products}
                </tbody>
              </table>
            </div></div><div className="absolute inset-0 pointer-events-none border border-black/5 rounded-xl dark:border-white/5"></div></div></div>
      </div>
      :
      <PageNotFound />
  )
}

export default AdminDashboard