import React from 'react'
import ProductCard from '../components/ProductCard';
import { useState, useEffect, useContext } from 'react'
import BannerSlider from '../components/BannerSlider';
// import ProductBanner from '../assets/img/product_banner.jpg'
// import UserContext from '../UserContext';


const Products = () => {

  // const { user } = useContext(UserContext);

  const [products, setProducts] = useState([])


  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/products/all`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        const filteredProducts = data.filter(product => product.isActive); // Filter out inactive products
        setProducts(filteredProducts.map(product => (
          <ProductCard key={product._id} productProp={product} />
        )));
      })
      .catch(error => console.error(error));
  }, []);


  return (
    <div>
      <BannerSlider />
      <h1 className='text-[2.5rem] mb-[1.5rem] text-left px-[2rem] font-semibold dark:text-gray-200'>All Products</h1>
      {/* <div className='flex justify-center py-5'> <img src={ProductBanner} /></div> */}
      <div className="grid gap-6 py-2 px-4 sm:grid-cols-2 lg:grid-cols-4">
        {products}
      </div>
    </div>
  )

}

export default Products