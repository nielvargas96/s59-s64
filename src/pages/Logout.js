import React from 'react'
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';


const Logout = () => {
  const { unsetUser, setUser } = useContext(UserContext);
  useEffect(() => {
    unsetUser();
    setUser({
      id: null,
      isAdmin: null
    });

  }, [])

  return (
    <Navigate to="/login" />
  )
}

export default Logout