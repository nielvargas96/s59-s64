import React from 'react'
import logo from '../assets/img/logo_gray.png'
import { Link, useNavigate } from 'react-router-dom'
import { useState, useEffect, useContext } from 'react';
import Swal2 from 'sweetalert2'
import PageNotFound from './PageNotFound';
import UserContext from '../UserContext';


const Register = () => {

  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [mobile, setMobile] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');

  const [isPasswordMatch, setIsPasswordMatch] = useState(true);
  const [isMobileLength, setIsMobileLength] = useState(true);

  useEffect(() => {
    if (mobile.length === 0) {
      setIsMobileLength(true)
    } else if (mobile.length !== 11) {
      setIsMobileLength(false)
    } else {
      setIsMobileLength(true)
    }
    setMobile(mobile.slice(0, 11));
  }, [mobile]);

  useEffect(() => {
    if (password !== password2) {
      setIsPasswordMatch(false)
    } else {
      setIsPasswordMatch(true)
    }
  }, [password, password2]);


  function createAccount(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_BACKEND_API}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstName: firstname,
        lastName: lastname,
        mobileNo: mobile,
        email: email,
        password: password
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          Swal2.fire({
            icon: `success`,
            title: `${data.message}`
          })
          navigate('/login');
        } else {
          Swal2.fire({
            icon: `error`,
            title: `${data.message}`,
            text: ''
          })
          setEmail('')
        }
      })
      .catch(error => {
        console.error('Error registering user:', error);
        navigate('/register')
      });
  }


  return (
    user.id === null || user.id === undefined
      ?
      <section >
        <div className="flex flex-col items-center justify-top px-6 py-8 mx-auto  lg:py-0">
          <Link to="/" className="flex text-[3rem] items-center mb-6 font-semibold text-gray-900 dark:text-white">
            <img width={240} src={logo} alt="logo" className='brightness-100 dark:brightness-[100]' />
          </Link>
          <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700 drop-shadow-[0_2px_8px_rgba(0,0,0,0.25)]">
            <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
              <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                Create an account
              </h1>
              <form onSubmit={(e) => createAccount(e)} className="space-y-4 md:space-y-6" action="#">
                <div>
                  <label htmlFor="firstname" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Firstname</label>
                  <input value={firstname} onChange={(e) => setFirstname(e.target.value)} type="text" name="firstname" id="firstname" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Enter your firstname" required />
                </div>
                <div>
                  <label htmlFor="lastname" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Lastname</label>
                  <input value={lastname} onChange={(e) => setLastname(e.target.value)} type="text" name="lastname" id="lastname" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Enter your lastname" required />
                </div>
                <div>
                  <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
                  <input value={email} onChange={(e) => setEmail(e.target.value)} type="email" name="email" id="email" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="example@mail.com" required />
                </div>
                <div>
                  <label htmlFor="mobile" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Mobile</label>
                  <input value={mobile} onChange={(e) => setMobile(e.target.value)} type="number" name="mobile" id="mobile" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Enter your mobile number" maxLength="11" required />
                  <small className='text-rose-500' hidden={isMobileLength}>Mobile number must be 11 characters</small>
                </div>
                <div>
                  <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
                  <input value={password} onChange={(e) => setPassword(e.target.value)} type="password" name="password" id="password" placeholder="••••••••" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required />
                </div>
                <div>
                  <label htmlFor="c-password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Confirm Password</label>
                  <input value={password2} onChange={(e) => setPassword2(e.target.value)} type="password" name="c-password" id="c-password" placeholder="••••••••" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required />
                  <small className='text-rose-500' hidden={isPasswordMatch}>Password does not match!</small>
                </div>

                <button type="submit" className="w-full text-white bg-blue-600 hover:bg-blue-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">Create an account</button>
                <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                  Already have an account? <Link className="font-medium text-blue-600 hover:underline dark:text-primary-500" to="/login">Sign in here</Link>
                </p>
              </form>
            </div>
          </div>
        </div>
      </section>
      :
      <PageNotFound />
  )
}

export default Register