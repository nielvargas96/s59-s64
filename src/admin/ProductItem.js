import React, { useState } from 'react'
import Swal2 from 'sweetalert2'

const ProductItem = ({ productProp }) => {

  const { _id, name, description, price, image, isActive, createdOn } = productProp;

  const formattedDate = new Date(createdOn).toLocaleDateString('en-US', {
    month: 'short',
    day: 'numeric',
    year: 'numeric'
  });

  const [productName, setProductName] = useState(name);
  const [productDescription, setProductDescription] = useState(description);
  const [productPrice, setProductPrice] = useState(price);
  const [productImage, setProductImage] = useState(image);


  // Archive Product
  function archiveProduct() {
    Swal2.fire({
      title: 'Are you sure?',
      text: 'This product will be archived.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, archive it',
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_BACKEND_API}/products/${_id}/archive`, {
          method: 'PUT',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        })
          .then(response => response.json())
          .then(data => {
            console.log(data);
          })
          .catch(error => {
            console.error('Error:', error);
            // Handle any errors that occur during the request
            Swal2.fire('Error', 'Failed to archive the product.', 'error');
          });
      }
    });
  }

  // Active Product
  function activateProduct() {
    fetch(`${process.env.REACT_APP_BACKEND_API}/products/${_id}/activate`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then(data => {
        console.log(data);
      })
      .catch(error => {
        console.error('Error:', error);
        // Handle any errors that occur during the request
        Swal2.fire('Error', 'Failed to archive the product.', 'error');
      });
  }

  // Edit Product
  function editProduct() {
    Swal2.fire({
      title: 'Edit Product',
      html: `
      <input style="width: 90% !important; margin: 10px 0 !important;" type="text" id="productName" class="swal2-input text-[1rem]" placeholder="Product Name" value="${productName}" required>
      <input style="width: 90% !important; margin: 10px 0 !important;" type="text" id="description" class="swal2-input text-[1rem]" placeholder="Description" value="${productDescription}" required>
      <input style="width: 90% !important; margin: 10px 0 !important;" type="number" id="price" class="swal2-input text-[1rem]" placeholder="Price" value="${productPrice}" required>
      <input style="width: 90% !important; margin: 10px 0 !important;" type="text" id="image" class="swal2-input text-[1rem] h-[2rem]" placeholder="Image URL" value="${productImage}" required>
    `,
      showCancelButton: true,
      confirmButtonText: 'Update',
      preConfirm: () => {
        const updatedProductName = document.getElementById('productName').value;
        const updatedDescription = document.getElementById('description').value;
        const updatedPrice = document.getElementById('price').value;
        const updatedImage = document.getElementById('image').value;

        // Check if all fields are filled
        if (updatedProductName && updatedDescription && updatedPrice && updatedImage) {
          setProductName(updatedProductName);
          setProductDescription(updatedDescription);
          setProductPrice(updatedPrice);
          setProductImage(updatedImage);
          // Make the API call to update the product with the updated values
          fetch(`${process.env.REACT_APP_BACKEND_API}/products/${_id}/edit`, {
            method: 'PUT',
            headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              name: updatedProductName,
              description: updatedDescription,
              price: updatedPrice,
              image: updatedImage
            })
          })
            .then(response => response.json())
            .then(data => {
              console.log(data);
              Swal2.fire('Updated', 'The product has been updated.', 'success');
            })
            .catch(error => {
              console.error('Error:', error);
              // Handle any errors that occur during the request
              Swal2.fire('Error', 'Failed to update the product.', 'error');
            });
        } else {
          Swal2.showValidationMessage('Please fill in all fields');
        }
      }
    });
  }

  function deleteProduct() {
    Swal2.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this product!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.isConfirmed) {
        // User confirmed the deletion
        fetch(`${process.env.REACT_APP_BACKEND_API}/products/${_id}`, {
          method: 'DELETE',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
          .then(response => response.json())
          .then(data => {
            if (data.success) {
              // Product deletion successful
              console.log('Product deleted:', data);
            } else {
              // Product deletion failed
              Swal2.fire('Error', 'Failed to delete product.', 'error');
              console.log('Failed to delete product:', data.message);
            }
          })
          .catch(error => {
            console.error('Error:', error);
            // Handle any errors that occur during the request
          });
      }
    });
  }



  return (
    <>
      <tr>
        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-900 dark:text-slate-400">{name}</td>
        <td className="border-b border-slate-100 dark:border-slate-700 p-4 text-slate-900 dark:text-slate-400">{description}</td>
        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">&#8369;{price}</td>
        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400 break-all"> <div className='flex   items-start'><img src={image} height={80} width={80} /> <div className='overflow-hidden ml-3 max-h-[60px] max-w-[200px]'>{image}</div></div> </td>
        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">{isActive === true ? 'Active' : 'Not Active'}</td>
        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">{formattedDate}</td>
        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">
          <div className='flex flex-row flex-wrap group relative cursor-pointer py-2'>
            <button onClick={() => editProduct()} className="w-full bg-transparent hover:bg-blue-500 text-blue-500 font-semibold hover:text-white py-1 px-4  mb-2 border border-blue-500 hover:border-transparent rounded" >Edit</button>
            <button onClick={() => deleteProduct()} className="w-full bg-transparent hover:bg-rose-500 text-rose-500 font-semibold hover:text-white py-1 px-4 mb-2 border border-rose-500 hover:border-transparent rounded">Delete</button>

            {isActive === true ?
              <button onClick={() => archiveProduct()} className="w-full bg-transparent hover:bg-orange-500 text-orange-500 mb-2 font-semibold hover:text-white py-1 px-4 border border-orange-500 hover:border-transparent rounded">Deactivate</button>
              :
              <button onClick={() => activateProduct()} className="w-full bg-transparent hover:bg-green-500 text-green-500 mb-2 font-semibold hover:text-white py-1 px-4 border border-green-500 hover:border-transparent rounded">Activate</button>
            }


            {/* <span className='menu-hover'>
              Actions
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                class="h-6 w-6"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                />
              </svg>
            </span>
            <div className='invisible absolute z-50 group-hover:visible'>
            
            </div> */}
          </div>

        </td>
      </tr >
    </>
  )
}

export default ProductItem