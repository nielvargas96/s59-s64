import React, { useContext, useEffect, useState } from 'react'
import UserContext from '../UserContext'
import PageNotFound from '../pages/PageNotFound';
import Swal2 from 'sweetalert2'

import { Link } from 'react-router-dom'


const RetrieveAllUsers = () => {
  const { user } = useContext(UserContext);
  // const [orderProducts, setOrderProducts] = useState([]);

  const [users, setUsers] = useState([]);

  function setUserAdmin(userId) {
    // console.log(userId)
    Swal2.fire({
      title: 'Set user as admin?',
      text: 'This user will be set as anadmin.',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Yes',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_BACKEND_API}/users/setAsAdmin/${userId}`, {
          method: 'PUT',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }, body: JSON.stringify({
            userId: userId
          })
        })
          .then(response => response.json())
          .then(data => {
            console.log(data);
          })
          .catch(error => {
            console.error('Error:', error);
            // Handle any errors that occur during the request
            Swal2.fire('Error', 'Failed to set admin.', 'error');
          });
      }
    });
  }


  // Set user as user
  function setUserAsUser(userId) {
    // console.log(userId)
    Swal2.fire({
      title: 'Set user as not admin?',
      text: 'This user will be set as an normal user.',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Yes',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_BACKEND_API}/users/setAsUser/${userId}`, {
          method: 'PUT',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }, body: JSON.stringify({
            userId: userId
          })
        })
          .then(response => response.json())
          .then(data => {
            console.log(data);
          })
          .catch(error => {
            console.error('Error:', error);
            // Handle any errors that occur during the request
            Swal2.fire('Error', 'Failed to set admin.', 'error');
          });
      }
    });
  }

  // Delete single user
  function deleteUser(userId) {
    Swal2.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this user!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Delete!',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.isConfirmed) {
        // User confirmed the deletion
        fetch(`${process.env.REACT_APP_BACKEND_API}/users/${userId}`, {
          method: 'DELETE',
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
          .then(response => response.json())
          .then(data => {
            if (data.success) {
              // User deletion successful
              console.log('User deleted:', data);
            } else {
              // User deletion failed
              Swal2.fire('Error', 'Failed to delete user.', 'error');
              console.log('Failed to delete user:', data.message);
            }
          })
          .catch(error => {
            console.error('Error:', error);
            // Handle any errors that occur during the request
          });
      }
    });
  }


  // Display all user
  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/users/allUsers`)
      .then(response => response.json())
      .then(data => {
        setUsers(data);
      })
  }, [users])




  return (
    user.isAdmin === true
      ?
      <>
        <div className="mt-4 -mb-3">
          <div className='flex justify-between items-center'>
            <h1 className="text-[2.5rem] mb-[1.5rem] text-left px-[2rem] font-semibold dark:text-gray-200">All Users</h1>
            <div className='ml-auto flex px-2'>
              <Link to='/admin' className="ml-auto flex items-center bg-emerald-700 hover:bg-emerald-600 text-white font-semibold hover:text-white py-1 px-4 mr-2 border border-emerald-500  rounded">
                <svg className="w-4 h-4 text-white dark:text-white mr-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="18" height="20" fill="none" viewBox="0 0 18 20">
                  <path stroke="currentColor" strokeLinecap="round" strokeWidth="2" d="M12 2h4a1 1 0 0 1 1 1v15a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h4m6 0v3H6V2m6 0a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1M5 5h8m-5 5h5m-8 0h.01M5 14h.01M8 14h5" />
                </svg> View Products
              </Link>

              <Link to='/retrieveOrders' className="ml-auto flex items-center bg-emerald-700 hover:bg-emerald-600 text-white font-semibold hover:text-white py-1 px-4 mr-2 border border-emerald-500  rounded">
                <svg className="w-4 h-4 text-white dark:text-white mr-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 19 20">
                  <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 15a2 2 0 1 0 0 4 2 2 0 0 0 0-4Zm0 0h8m-8 0-1-4m9 4a2 2 0 1 0 0 4 2 2 0 0 0 0-4Zm1-4H5m0 0L3 4m0 0h5.501M3 4l-.792-3H1m11 3h6m-3 3V1" />
                </svg> Orders
              </Link>

              <Link to='/retrieveUsers' className="ml-auto flex items-center bg-emerald-700 hover:bg-emerald-600 text-white font-semibold hover:text-white py-1 px-4 mr-2 border border-emerald-500  rounded">
                <svg className="w-4 h-4 mr-2 text-white dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 19"> <path d="M14.5 0A3.987 3.987 0 0 0 11 2.1a4.977 4.977 0 0 1 3.9 5.858A3.989 3.989 0 0 0 14.5 0ZM9 13h2a4 4 0 0 1 4 4v2H5v-2a4 4 0 0 1 4-4Z" /> <path d="M5 19h10v-2a4 4 0 0 0-4-4H9a4 4 0 0 0-4 4v2ZM5 7a5.008 5.008 0 0 1 4-4.9 3.988 3.988 0 1 0-3.9 5.859A4.974 4.974 0 0 1 5 7Zm5 3a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm5-1h-.424a5.016 5.016 0 0 1-1.942 2.232A6.007 6.007 0 0 1 17 17h2a1 1 0 0 0 1-1v-2a5.006 5.006 0 0 0-5-5ZM5.424 9H5a5.006 5.006 0 0 0-5 5v2a1 1 0 0 0 1 1h2a6.007 6.007 0 0 1 4.366-5.768A5.016 5.016 0 0 1 5.424 9Z" /> </svg> Users
              </Link>

            </div>
          </div>
          <div className="not-prose relative bg-slate-50 rounded-xl overflow-hidden dark:bg-slate-800/25">
            <div className="relative rounded-xl overflow-auto">
              <div className="shadow-sm overflow-hidden mt-4">
                <table className="border-collapse table-fixed w-full text-sm">
                  <thead>
                    <tr>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">ID</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Fullname</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Email</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={100}>Mobile</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={300}>Password</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={100}>Admin</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={200}>Action</th>
                    </tr>
                  </thead>
                  <tbody className="bg-white dark:bg-slate-800">

                    {users.map((userData, index) => (
                      <tr key={userData._id}>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-900 dark:text-slate-400">
                          <p className='break-all'>{userData._id}</p>
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-900 dark:text-slate-400">
                          {/* <p>ID: {userData._id}</p> */}
                          <p>{userData.lastName}, {userData.firstName} </p>
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 text-slate-900 dark:text-slate-400">
                          {userData.email}
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">
                          {userData.mobileNo}
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400 break-all">
                          {userData.password}<br />
                          {/* <span className='p-1 bg-green-800 w-auto cursor-pointer text-white'>Show</span> */}
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">
                          {userData.isAdmin ? 'Admin' : 'User'}
                        </td>

                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">
                          {(index === 0) ? 'Unavailable'
                            :
                            <div className='flex flex-row flex-wrap group relative cursor-pointer py-2'>
                              {/* <button onClick={() => editUser()} className="w-auto bg-transparent hover:bg-blue-500 text-blue-500 font-semibold hover:text-white py-1 px-4 mr-2 mb-2 border border-blue-500 hover:border-transparent rounded" >Edit</button> */}
                              <button onClick={() => deleteUser(userData._id)} className="w-auto bg-transparent hover:bg-rose-500 text-rose-500 font-semibold hover:text-white py-1 px-4 mb-2 border border-rose-500 hover:border-transparent rounded">Delete</button>
                              {
                                userData.isAdmin ?
                                  <button onClick={() => setUserAsUser(userData._id)} className="w-auto bg-transparent hover:bg-green-500 text-green-500 mb-2 font-semibold hover:text-white py-1 px-4 border border-green-500 hover:border-transparent rounded">Set as User</button>
                                  :
                                  <button onClick={() => setUserAdmin(userData._id)} className="w-auto bg-transparent hover:bg-green-500 text-green-500 mb-2 font-semibold hover:text-white py-1 px-4 border border-green-500 hover:border-transparent rounded">Set as Admin</button>
                              }

                            </div>
                          }
                        </td>
                      </tr >

                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div >
      </>
      :
      <PageNotFound />
  )
}

export default RetrieveAllUsers