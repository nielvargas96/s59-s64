import React, { useContext, useEffect, useState } from 'react'
import UserContext from '../UserContext'
import PageNotFound from '../pages/PageNotFound';
import { Link } from 'react-router-dom'

const RetrieveAllOrders = () => {
  const { user } = useContext(UserContext);

  // const [orderProducts, setOrderProducts] = useState([]);

  // const [users, setUsers] = useState([]);

  const [orders, setOrders] = useState([]);


  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/users/allUsers`)
      .then(response => response.json())
      .then(data => {
        // console.log(data.user.name)

        // Extract all order products from users' orderProducts array
        const allOrders = data.reduce((acc, user) => [...acc, ...user.orderProducts.map(order => ({ ...order, user: user }))], []);
        setOrders(allOrders);
        console.log(allOrders)

        // console.log(...data.user)
        // const allCustomerUsers = data.()
      })
  }, []);


  return (
    user.isAdmin === true
      ?
      <>
        <div className="mt-4 mb-3">
          <div className='flex justify-between items-center'>
            <h1 className="text-[2.5rem] mb-[1.5rem] text-left px-[2rem] font-semibold dark:text-gray-200">Customer Orders</h1>
            <div className='ml-auto flex px-2'>
              <Link to='/admin' className="ml-auto flex items-center bg-emerald-700 hover:bg-emerald-600 text-white font-semibold hover:text-white py-1 px-4 mr-2 border border-emerald-500  rounded">
                <svg className="w-4 h-4 text-white dark:text-white mr-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 19 20">
                  <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 15a2 2 0 1 0 0 4 2 2 0 0 0 0-4Zm0 0h8m-8 0-1-4m9 4a2 2 0 1 0 0 4 2 2 0 0 0 0-4Zm1-4H5m0 0L3 4m0 0h5.501M3 4l-.792-3H1m11 3h6m-3 3V1" />
                </svg> View Products
              </Link>

              <Link to='/retrieveOrders' className="ml-auto flex items-center bg-emerald-700 hover:bg-emerald-600 text-white font-semibold hover:text-white py-1 px-4 mr-2 border border-emerald-500  rounded">
                <svg className="w-4 h-4 text-white dark:text-white mr-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 19 20">
                  <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 15a2 2 0 1 0 0 4 2 2 0 0 0 0-4Zm0 0h8m-8 0-1-4m9 4a2 2 0 1 0 0 4 2 2 0 0 0 0-4Zm1-4H5m0 0L3 4m0 0h5.501M3 4l-.792-3H1m11 3h6m-3 3V1" />
                </svg> Orders
              </Link>

              <Link to='/retrieveUsers' className="ml-auto flex items-center bg-emerald-700 hover:bg-emerald-600 text-white font-semibold hover:text-white py-1 px-4 mr-2 border border-emerald-500  rounded">
                <svg className="w-4 h-4 mr-2 text-white dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 19"> <path d="M14.5 0A3.987 3.987 0 0 0 11 2.1a4.977 4.977 0 0 1 3.9 5.858A3.989 3.989 0 0 0 14.5 0ZM9 13h2a4 4 0 0 1 4 4v2H5v-2a4 4 0 0 1 4-4Z" /> <path d="M5 19h10v-2a4 4 0 0 0-4-4H9a4 4 0 0 0-4 4v2ZM5 7a5.008 5.008 0 0 1 4-4.9 3.988 3.988 0 1 0-3.9 5.859A4.974 4.974 0 0 1 5 7Zm5 3a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm5-1h-.424a5.016 5.016 0 0 1-1.942 2.232A6.007 6.007 0 0 1 17 17h2a1 1 0 0 0 1-1v-2a5.006 5.006 0 0 0-5-5ZM5.424 9H5a5.006 5.006 0 0 0-5 5v2a1 1 0 0 0 1 1h2a6.007 6.007 0 0 1 4.366-5.768A5.016 5.016 0 0 1 5.424 9Z" /> </svg> Users
              </Link>

            </div>
          </div>
          <div className="not-prose relative bg-slate-50 rounded-xl overflow-hidden dark:bg-slate-800/25">
            <div className="relative rounded-xl overflow-auto">
              <div className="shadow-sm overflow-hidden mt-4">
                <table className="border-collapse table-fixed w-full text-sm">
                  <thead>
                    <tr>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left" width={50}>No.</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Customer Details</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Product Name</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Price</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Quantity</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Ordered Date</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Status</th>
                      <th className="border-b-2 dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-900 dark:text-slate-200 text-left">Total</th>
                    </tr>
                  </thead>
                  <tbody className="bg-white dark:bg-slate-800">
                    {orders.map((order, index) => (
                      <tr key={index}>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-900 dark:text-slate-400" width={50}>
                          {index}
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-900 dark:text-slate-400">
                          {order.user.lastName}, {order.user.firstName} <br /> {order.user.email}
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-900 dark:text-slate-400">
                          {order.name}
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 text-slate-900 dark:text-slate-400">
                          {order.price}
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">
                          {order.quantity}
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">
                          {new Date(order.orderedDate).toLocaleString()}
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">
                          {order.status}
                        </td>
                        <td className="border-b border-slate-100 dark:border-slate-700 p-4 pr-8 text-slate-900 dark:text-slate-400">
                          {order.quantity * order.price}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </>
      :
      <PageNotFound />
  )
}

export default RetrieveAllOrders